1. [Веб-вёрстка. Базовый уровень](https://gitlab.com/sergey.isanin/weblayout)
2. [Javascript. Базовый уровень](https://gitlab.com/sergey.isanin/javascript)
3. [Vue.js](https://gitlab.com/sergey.isanin/vue_technozavrrr) [★ CRM](https://gitlab.com/sergey.isanin/crm_final_vue)
4. [Система контроля версий Git](https://gitlab.com/sergey.isanin/sergey.isanin/-/blob/main/structure/git.md)
5. [Веб-вёрстка. Продвинутый уровень](https://gitlab.com/sergey.isanin/weblayout_pro) [★ С промежуточной аттестацией](https://gitlab.com/sergey.isanin/final_weblayout_pro)
6. [Javascript. Продвинутый уровень](https://gitlab.com/sergey.isanin/javascript_pro) [★ COIN](https://gitlab.com/sergey.isanin/final_javascript_pro)
7. [★ Итоговая аттестация](https://gitlab.com/sergey.isanin/exam_vue)

В целях ознакомления предоставлена структура курса и техническое задание к каждому модулю.
★ - С промежуточной аттестацией

Копия моих репозиториев из [GitLub](https://gitlab.skillbox.ru/sergey_isanin)

---

<details>
  <br>
  <summary>Технические задания</summary>

[Веб-вёрстка. Продвинутый уровень. Макет](https://www.figma.com/file/lBMYXRH6CHIoiIK6xrcibD)

[Javascript. Продвинутый уровень](https://gitlab.com/sergey.isanin/final_javascript_pro/-/blob/main/spec)
[Макет](https://www.figma.com/file/JUJVDoP27x18v4Eqt66SdK/Bank-Diploma?node-id=1%3A3)

[Итоговая аттестация](https://gitlab.com/sergey.isanin/exam_vue/-/blob/main/spec)

<!-- [★ secret](https://www.figma.com/file/JUJVDoP27x18v4Eqt66SdK/Bank-Diploma?node-id) -->

</details>
