Содержание

1.  Работаем с Git на своём компьютере

        1.1 Интро к курсу
        1 мин. 1.2 Интро
        8 мин. 1.3 Зачем нужна система контроля версий
        5 мин. 1.4 Как выглядит работа с Git
        4 мин. 1.5 Практическая работа. Установка Git на Windows
        4 мин. 1.6 Практическая работа. Установка Git на MacOS
        4 мин. 1.7 Практическая работа. Установка Git на Linux
        8 мин. 1.8 Практическая работа. Работа с Git в командной строке
        9 мин. 1.9 Жизненный цикл файлов в репозитории
        12 мин. 1.10 Игнорирование изменений
        14 мин. 1.11 Работа с Git в средах разработки
        1 мин. 1.12 Итоги
        1.13 Тест

2.  Работаем с удалённым репозиторием

        1 мин. 2.1 Интро
        3 мин. 2.2 Удалённые и локальные репозитории
        7 мин. 2.3 GitHub и GitLab: обзор систем
        4 мин. 2.4 Регистрация на GitHub и создание удалённого репозитория
        12 мин. 2.5 Практическая работа. Подключение к удалённому репозиторию
        4 мин. 2.6 Внесение изменений и их отправка
        6 мин. 2.7 Практическая работа. Обновление локального репозитория
        9 мин. 2.8 Практическая работа. Разрешение конфликтов при обновлении репозитория
        1 мин. 2.9 Итоги
        2.10 Тест

3.  Командная работа в Git

        2 мин. 3.1 Интро
        11 мин. 3.2 Практическая работа. Ветки: создание и работа
        12 мин. 3.3 Практическая работа. Работа с удалёнными ветками
        9 мин. 3.4 Практическая работа. Откладывание изменений
        8 мин. 3.5 Слияние веток
        6 мин. 3.6 Практическая работа. Разрешение конфликтов
        8 мин. 3.7 Запрос на слияние (pull request)
        5 мин. 3.8 Модели ветвления
        1 мин. 3.9 Итоги
        3.10 Тест

4.  Сравнение версий и отмена изменений

        1 мин. 4.1 Интро
        13 мин. 4.2 Практическая работа. Просмотр изменений
        10 мин. 4.3 Практическая работа. Удаление незакоммиченных изменений
        10 мин. 4.4 Отмена закоммиченных изменений
        9 мин. 4.5 Практическая работа. Отмена (сброс) коммитов
        8 мин. 4.6 Практическая работа. Отмена слияний, переименование и удаление веток
        1 мин. 4.7 Итоги
        4.8 Тест

5.  Инструменты и правила работы с Git

        13 мин. 5.1 Практическая работа. Работа в командной строке
        8 мин. 5.2 Практическая работа. Работа в GitHub
        7 мин. 5.3 Работа в GitLab
        6 мин. 5.4 Практическая работа. Работа в IDE от JetBrains
        7 мин. 5.5 Практическая работа. Работа в VS Code
        5 мин. 5.6 Другие клиенты и инструменты
        7 мин. 5.7 Правила работы с Git
        19 мин. 5.8 Частые проблемы и их решения
        5.9 Тест
